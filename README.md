## How To Install

Clone this repo and do the following steps in sequence: 

1. Copy .env.example file to .env 
2. Add this variable to .env (update value if needed)
```
CENTRAL_DOMAIN=localhost
CURRENT_DOMAIN=localhost 
``` 
3. Update database credential inside .env
4. Run `composer install`  
5. Run `php artisan key:generate` 
6. Run `php artisan migrate` 
7. Setup tenant that you want to use  
8. Run `php artisan tenants:migrate` to migrate table in all tenants database  
9. Run `php artisan db:seed` to add basic user credential 
```
{ 
    "email" : "admin@qerja.io" 
    "pass" : "rahasia123"
}
```
8. Run `php artisan serve` or whatever you use
