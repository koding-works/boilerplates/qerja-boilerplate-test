<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Inertia\Response;

class BaseController extends Controller
{
    protected string $title; /*Title Module For View*/
    protected string $path = ''; /*Path Location Module*/
    protected string $page = ''; /*Path Location Module*/
    protected string $module; /*Name Module*/
    protected $data; /*Data Object*/

    /**
     *
     * @return Response
     */
    public function index() : Response
    {
        return Inertia::render($this->path.$this->module . '/Index', [
            "title" => $this->title,
            "module" => $this->module,
            "data" => $this->data
        ]);
    }
}
