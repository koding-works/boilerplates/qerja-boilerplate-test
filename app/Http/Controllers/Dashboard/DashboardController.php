<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\BaseController;

class DashboardController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = "Dashboard";
        $this->module = "dashboard";
    }
}
