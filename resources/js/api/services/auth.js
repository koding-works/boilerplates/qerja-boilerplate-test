import { axiosInstance } from "./config/axios";

export const postLogin = async(body) => {
    const response = await axiosInstance
        .post(route('submit.login'), body)
        .then((res) => res.data)
    return response
};

export const postLogout = async() => {
    const response = await axiosInstance
        .post(route('logout'))
        .then((res) => res.data)
    return response
};
