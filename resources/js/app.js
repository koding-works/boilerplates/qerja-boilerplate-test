import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import Layout from './layout/App';

createInertiaApp({
    resolve: (name) => {
        const page = require(`./pages/${name}`).default;
        if (page.layout === undefined) {
            page.layout = Layout
        }
        return page;
    },
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .mixin({methods : {route: window.route  }})
            .mount(el);
    },
});
