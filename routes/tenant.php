<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\DashboardController;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    // Authentication
    Route::namespace('Auth')->group(function () {
        Route::get('/signin', 'LoginController@showLoginPage')->name('login');
        Route::post('signin', 'LoginController@login')->name('submit.login');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });

    // Dashboard
    Route::namespace('Dashboard')->middleware(['auth'])->group(function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
    });
});
