<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication
Route::namespace('Auth')->group(function () {
    Route::get('/signin', 'LoginController@showLoginPage')->name('login');
    Route::post('signin', 'LoginController@login')->name('submit.login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

// Dashboard
Route::namespace('Dashboard')->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
});
